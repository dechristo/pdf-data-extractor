using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PdfDataExtractor;

namespace PdfDataExtractor.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestConstructorThrowsArgumentExceptionIfFilenameIsEmpty()
        {
            PdfExtractor pdfde = new PdfExtractor(string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestConstructorThrowsExceptionForNonExistentFile()
        {
            PdfExtractor pdfde = new PdfExtractor("test2.pdf");
        }

        [TestMethod]
        public void TestConstructorReturnsPdfExtractorInstanceForValidFile()
        {
            PdfExtractor pdfde = new PdfExtractor("test.pdf");

            Assert.IsInstanceOfType(pdfde, typeof(PdfExtractor));
        }

        [TestMethod]
        public void TestExtractReturnsStringList()
        {
            PdfExtractor pdfde = new PdfExtractor("test.pdf");
            List<string> extracted = pdfde.Extract();

            Assert.IsNotNull(extracted);
            Assert.AreEqual(extracted.Count, 6);
            Assert.AreEqual(extracted[0], "Table 1");
            Assert.AreEqual(extracted[1], "Transport Travels Books Food");
            Assert.AreEqual(extracted[2], "March 81,00 1.200,00 56,00 300,00");
            Assert.AreEqual(extracted[3], "April 89,00 0,00 87,00 200,00");
            Assert.AreEqual(extracted[4], "June 0,00 400,00 0,00 136,00");
            Assert.AreEqual(extracted[5], "\01");
        }
    }
}
