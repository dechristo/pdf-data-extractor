﻿using System;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Collections.Generic;
using System.Text;

namespace PdfDataExtractor
{
    public class PdfExtractor
    {
        private PdfReader _reader;
        private ITextExtractionStrategy _ites;
        private List<string> extracted;

        public PdfExtractor(string filename)
        {
            if (string.Empty == filename)
            {
                throw new ArgumentException("A file name must be provided!");
            }

            try
            {
                this._reader = new PdfReader(filename);
				this._ites = new SimpleTextExtractionStrategy();
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[ERROR]: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public List<string> Extract()
        {
            if (this.extracted != null) return this.extracted;

            string rawData = this.GetRawData();
            string[] data = rawData.Split('\n');
            this.extracted = new List<string>();

            foreach (string line in data)
            {
                extracted.Add(line);
            }
            return extracted;
        }

        public void Print()
        {
            foreach(string line in this.extracted)
            {
                Console.WriteLine(line);
                Console.WriteLine("-------------------------------");
            }
        }

        private string GetRawData()
        {
            string rawData = string.Empty;

            for (int page = 1; page <= this._reader.NumberOfPages; page++)
            {
                String extractedRaw = PdfTextExtractor.GetTextFromPage(
                    this._reader, 
                    page, 
                    this._ites
                );

                extractedRaw = Encoding.UTF8.GetString(Encoding.Convert(
                    Encoding.Default,
                    Encoding.UTF8,
                    Encoding.Default.GetBytes(extractedRaw)
                ));
                rawData = rawData + extractedRaw;
                this._reader.Close();
            }

            return rawData;
        }
    }
}
