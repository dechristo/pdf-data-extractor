﻿using System;

namespace PdfDataExtractor
{
    class Program
    {
        static void Main(string[] args)
        {
            PdfExtractor e = new PdfExtractor("test.pdf");
            e.Extract();
            e.Print();
        }
    }
}
